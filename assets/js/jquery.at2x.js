/**
 * @package   jQuery.at2x
 * @author    Fabian Perrey
 * @license   GNU
 * @copyright IXTENSA
 *
 * a small jQuery plugin to replace background images and regular images
 * for each given element with a hi-res version named with the given suffix
 */

;(function( $ ) {
    $.fn.extend({

        // Replacing background-image of elements
        bgAt2x: function( options ) {

            var settings = $.extend({
                suffix:     '@2x',
                ratio:      '2',
                className:  'at2x',
                devMode:    false
            }, options );

            if (typeof console === "undefined") {
                console = {
                    log: function() { }
                };
            }

            var  medQuery = '(-webkit-min-device-pixel-ratio: '+settings.ratio+'),(min-resolution: '+settings.ratio+'dppx)';

            if ( window.devicePixelRatio > 1 || ( window.matchMedia && window.matchMedia( settings.medQuery ).matches || settings.devMode )) {

                return this.each( function() {

                    var elem = $(this);
                    var bgUrl = elem.css('background-image');
                    var extension = bgUrl.substr((bgUrl.lastIndexOf('.')));
                    var bgNewUrl = bgUrl.replace( extension, settings.suffix + extension );
                    
                    if (bgUrl == 'none') {
                        console.log("bgAt2x: your element has no css background-image");
                    }
                    else if (bgUrl.lastIndexOf(settings.className) >= 0) {
                        console.log("bgAt2x: background-image has already been replaced");
                    }
                    else
                    {
                        $.ajax({
                            url: bgNewUrl.replace(/url\(\"|url\(|\"\)|\)/g, ''),
                            type:'HEAD',
                            success: function()
                            {
                                elem.css({ 'background-image': bgNewUrl }).addClass(settings.className);
                            },
                            error: function() {
                                console.log(bgNewUrl.replace(/url\(\"|url\(|\"\)|\)/g, '')+' not found.');
                            }
                        });
                    }
                });
            }
        },

        // Replacing src of img elements
        imgAt2x: function( options ) {

            var settings = $.extend({
                suffix:     '@2x',
                ratio:      '2',
                className:  'at2x',
                devMode:    false
            }, options );

            if (typeof console === "undefined") {
                console = {
                    log: function() { }
                };
            }

            var  medQuery = '(-webkit-min-device-pixel-ratio: '+settings.ratio+'),(min-resolution: '+settings.ratio+'dppx)';

            if ( window.devicePixelRatio > 1 || ( window.matchMedia && window.matchMedia( settings.medQuery ).matches || settings.devMode )) {

                return this.each( function() {

                    var elem = $(this);
                    var imgUrl = elem.attr('src');
                    
                    if (typeof imgUrl === 'undefined')
                    {
                        console.error("imgAt2x: element has no src attribute");
                    }
                    else if (imgUrl.lastIndexOf(settings.className) >= 0 )
                    {
                        console.log("imgAt2x: image has already been replaced");
                    }
                    else
                    {
                        var extension = imgUrl.substr((imgUrl.lastIndexOf('.')));
                        var imgNewUrl = imgUrl.replace( extension, settings.suffix + extension );

                        $.ajax({
                            url: imgNewUrl,
                            type:'HEAD',
                            success: function()
                            {
                                elem.attr({ src: imgNewUrl }).addClass(settings.className);
                            },
                            error: function() {
                                console.log(imgNewUrl.replace(/url\(\"|url\(|\"\)|\)/g, '')+' not found.');
                            }
                        });
                    }       
                });
            }
        }
    });
})(jQuery);
