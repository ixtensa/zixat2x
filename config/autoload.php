<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2013 Leo Feyer
 *
 * @package   jQuery.at2x
 * @author    Fabian Perrey
 * @license   GNU
 * @copyright IXTENSA
 */

/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'ce_image_bgAt2x'	=> 'system/modules/zixAt2x/templates',
	'ce_text_bgAt2x'	=> 'system/modules/zixAt2x/templates',
	'j_at2x'			=> 'system/modules/zixAt2x/templates'
));
