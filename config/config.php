<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2013 Leo Feyer
 *
 * @package   jQuery.at2x
 * @author    Fabian Perrey
 * @license   GNU
 * @copyright IXTENSA
 */

if (TL_MODE == 'FE') {
	$GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/zixAt2x/assets/js/jquery.at2x.min.js|static';
}
