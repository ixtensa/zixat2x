# jQuery.at2x

* Copyright: Ixtensa (http://wwww.ixtensa.de)
* Author: Fabian Perrey (f.perrey@gmx.de)
* License: LGPL


### System requirements
* jQuery 1.7


### Beschreibung
The jQuery plugin replaces images and backgrounds with hi-res versions. It extends the file path of the orignal image with the suffix, checks the server via ajax if the file exits and replaces it of so.
The plugin itself (jquery.at2x.js) can also bu used idependently 



### Getting started
Elements with the classes "bgAt2x" or "imgAt2x" will be initiated automatically (j_at2x needs to be activated within the page layout). 
The plugin can also be applied to every element in the DOM manually:den:

> $('.background_container').bgAt2x();
> $('#header img').imgAt2x();

Folgende Standard-Optionen können beim Aurufen angepasst werden:
Following default options can be changed when calling the plugin:

> suffix:      '@2x'   -> Das suffix für die Hi-Res Bildatei
> ratio:       '2'     -> die Pixelratio, ab der ein Austausch vorgenommen werden soll
> className:   'at2x'  -> Klassenname, der dem Element nach Autausch des images vergeben wird
> devMode:     false   -> Forciert eine Ausagbe des Hi-Res Bildes unabhaengig von der Pixelratio

Examples:

> $('.background_container').bgAt2x({
>   suffix:     '-3x',
>   ratio:      '3',
>   className:  'at3x',
>   devMode:    true
> });

> $('img.at2x').imgAt2x({
>   suffix:     '-3x',
>   ratio:      '3',
>   className:  'at3x',
>   devMode:    true
> });
